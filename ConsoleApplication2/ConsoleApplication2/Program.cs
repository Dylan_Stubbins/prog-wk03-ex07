﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var words = "Hello my name is stephen and I like to dance";

            var set = words.Split(' ');

            foreach (var word in set)
            {
                Console.WriteLine(word);
            }
        }
    }
}
